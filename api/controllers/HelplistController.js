/**
 * HelplistController
 *
 * @description :: Server-side logic for managing Helplists
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    registration: function(req, res) {
        var loginVal = req.body.login;
        var passwordVal = req.body.password;
        var roleVal = req.body.role;
        HelplistService.registration(loginVal, passwordVal, roleVal, function(success) {
            req.session.user = success;
            req.session.authenticated = true;
            req.session.save();
            res.json(success);
        });
    },
    login: function(req, res) {
        var loginVal = req.body.login;
        var passwordVal = req.body.password;
        HelplistService.login(loginVal, passwordVal, function(success) {
            req.session.user = success;
            req.session.authenticated = true;
            req.session.save();
            res.json(success);
        });
    },
    logout: function(req, res) {
        delete req.session.user;
        delete req.session.authenticated;
        res.json('ok');
    },
	getProblems: function(req, res) {
        HelplistService.getProblems(function(problems) {
            res.json(problems);
        });
    },
    addProblem: function(req, res) {
        var problemVal = req.body.problem;
        var solutionVal = req.body.solution;
        HelplistService.addProblem(problemVal, solutionVal, req.session.user.id, function(success) {
            res.json(success);
        });
    },
    editProblemSolution: function(req, res) {
        var problemVal = req.body.problem;
        var solutionVal = req.body.solution;
        var old_problem = req.body.old_problem;
        HelplistService.editProblemSolution(problemVal, solutionVal, old_problem, function(success) {
            res.json(success);
        });
    },
    setRating: function(req, res) {
        var ratingVal = req.body.rating;
        var problemVal = req.body.problem;
        HelplistService.setRating(ratingVal, problemVal, function(success) {
            res.json(success);
        });
    },
};

