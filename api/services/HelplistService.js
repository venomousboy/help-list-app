var crypto = require('crypto');

module.exports = {
  registration: function(loginVal, passwordVal, roleVal, next) {
    var clearLogin = (typeof loginVal.value == 'undefined') ? null : this.cleanStr(loginVal.value, 'auth');
    var clearPassword = (typeof passwordVal.value == 'undefined') ? null : this.cleanStr(passwordVal.value, 'auth');
    if (roleVal.value == 'editor') var role = 'editor';
    if (roleVal.value == 'rater') var role = 'rater';

    login = clearLogin;
    password = crypto.createHash('sha1')
      .update(clearPassword)
      .update(clearLogin)
      .digest('hex');

    User.create({login: login, password: password, role: role}).exec(function(err, user) {      
      if(err) throw err;
      next(user);
    });
  },

  login: function(loginVal, passwordVal, next) {
    var clearLogin = (typeof loginVal.value == 'undefined') ? null : this.cleanStr(loginVal.value, 'auth');
    var clearPassword = (typeof passwordVal.value == 'undefined') ? null : this.cleanStr(passwordVal.value, 'auth');

    login = clearLogin;
    password = crypto.createHash('sha1')
      .update(clearPassword)
      .update(clearLogin)
      .digest('hex');
    
    User.findOne({login: login, password: password}).exec(function(err, user) {
      if(err) throw err;
      next(user);
    });
  },

  getProblems: function(next) {
    Helplist.find().exec(function(err, problems) {
      if(err) throw err;
      next(problems);
    });
  },

  addProblem: function(problemVal, solutionVal, user_id, next) {
    var problem = (typeof problemVal.value == 'undefined') ? null : this.cleanStr(problemVal.value);
    var solution = (typeof solutionVal.value == 'undefined') ? null : this.cleanStr(solutionVal.value);
    Helplist.create({problem: problem, solution: solution, user_id: user_id, rating: 1}).exec(function(err, problem) {
      if(err) throw err;
      next(problem);
    });
  },

  editProblemSolution: function(problemVal, solutionVal, old_problem, next) {
    var problem = (typeof problemVal.value == 'undefined') ? null : this.cleanStr(problemVal.value);
    var solution = (typeof solutionVal.value == 'undefined') ? null : this.cleanStr(solutionVal.value);
    var old_problem_cl = (typeof old_problem == 'undefined') ? null : this.cleanStr(old_problem);

    Helplist.update({problem: old_problem_cl},{problem: problem, solution: solution}).exec(function(err, problem){
    if(err) throw err;
      next(problem);
    });
  },

  setRating: function(ratingVal, problemVal, next) {
    var rating = parseInt(ratingVal);
    var problem = (typeof problemVal == 'undefined') ? null : this.cleanStr(problemVal);

    Helplist.update({problem: problem},{rating: rating}).exec(function(err, problem){
    if(err) throw err;
      next(problem);
    });
  },

  cleanStr: function(str, method = 'form') {
    var re = (method == 'auth') ? /([^a-zA-Z0-9]+)/gi : /([^a-zA-Zа-яА-Я0-9-_()\.\s]+)/gi; 
    var cleanString = str.replace(re, "");
    return cleanString;
  },
};