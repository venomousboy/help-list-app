helpListApp.service('HelplistService', function($http, $q) {
  return {
    'registration': function(login, password, role) {
        var defer = $q.defer();
        $http.post('/helplist/registration', {"login": login, "password": password, "role": role}).success(function(resp){
          defer.resolve(resp);
        }).error( function(err) {
          defer.reject(err);
        });
        return defer.promise;
    },
    'login': function(login, password) {
      var defer = $q.defer();
      $http.post('/helplist/login', {"login": login, "password": password}).success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'logout': function() {
      var defer = $q.defer();
      $http.post('/helplist/logout').success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'getProblems': function() {
      var defer = $q.defer();
      $http.get('/helplist/getProblems').success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'addProblem': function(problem, solution) {
      var defer = $q.defer();
      $http.post('/helplist/addProblem', {"problem": problem, "solution": solution}).success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'editProblemSolution': function(problem, solution, old_problem) {
      var defer = $q.defer();
      $http.post('/helplist/editProblemSolution', {"problem": problem, "solution": solution, "old_problem": old_problem}).success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'setRating': function(rating, problem) {
      var defer = $q.defer();
      $http.post('/helplist/setRating', {"rating": rating, "problem": problem}).success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
}});