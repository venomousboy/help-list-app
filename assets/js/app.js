'use strict';

var helpListApp = angular.module('helpListApp', ['ngRoute', 'ui.bootstrap']);
helpListApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.when('/', {
      templateUrl: '/templates/helplist.html',
      controller: 'HelplistCtrl'
    }).otherwise({
      redirectTo: '/',
      caseInsensitiveMatch: true
    })
  }]);

helpListApp.controller('HelplistCtrl', ['$scope', '$rootScope', 'HelplistService', function($scope, $rootScope, HelplistService) {

  $scope.problem = {};
  $scope.solution = {};
  $scope.e_problem = {};
  $scope.e_solution = {};
  $scope.problems = [];

  $scope.editing = false;
  $scope.create = false;

  $scope.login = {};
  $scope.password = {};
  $scope.reg_login = {};
  $scope.reg_password = {};
  $scope.reg_role = {};

  $scope.user = window.user || false;

  $scope.editor = $scope.user.role == 'editor' ? true : false; 
  $scope.rater = $scope.user.role == 'rater' ? true : false; 

  $scope.signin = function() {
    HelplistService.login($scope.login, $scope.password).then(function(response) {
      $scope.user = response;
      $scope.editor = $scope.user.role == 'editor' ? true : false; 
      $scope.rater = $scope.user.role == 'rater' ? true : false; 
    });
  }

  $scope.registration = function() {
    HelplistService.registration($scope.reg_login, $scope.reg_password, $scope.reg_role).then(function(response) {
      $scope.user = response;
      $scope.editor = $scope.user.role == 'editor' ? true : false; 
      $scope.rater = $scope.user.role == 'rater' ? true : false; 
    });
  }  

  $scope.logout = function() {
    HelplistService.logout().then(function(response) {
      $scope.user = response;
      window.location.reload();
    });
  }

  var getProblems = function() {
    HelplistService.getProblems().then(function(response) {
      $scope.problems = response;
    });
  }

  getProblems();

  $scope.addProblem = function() {
    HelplistService.addProblem($scope.problem, $scope.solution).then(function(response) {
      $scope.problems.push($scope.problem);
      $scope.problem = {};
      $scope.solution = {};
      getProblems();
    });
  }

  $scope.editProblemSolution = function(problem) {
    HelplistService.editProblemSolution($scope.e_problem, $scope.e_solution, problem).then(function(response) {
      $scope.e_problem = {};
      $scope.e_solution = {};
      getProblems();
    });
  }

  $scope.ratings = [
  {
      max: 5
  }];

  $scope.getSelectedRating = function (rating) {
      console.log(rating);
  }

  $scope.setRating = function(rating, problem) {
    HelplistService.setRating(rating, problem).then(function(response) {
      getProblems();
    });
  }

}]);

helpListApp.directive('starRating', function () {
    return {
        restrict: 'A',
        template: '<ul class="rating">' +
            '<li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">' +
            '\u2605' +
            '</li>' +
            '</ul>',
        scope: {
            ratingValue: '=',
            max: '=',
            onRatingSelected: '&'
        },
        link: function (scope, elem, attrs) {

            var updateStars = function () {
                scope.stars = [];
                for (var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
            };

            scope.toggle = function (index) {
                scope.ratingValue = index + 1;
                scope.onRatingSelected({
                    rating: index + 1
                });
            };

            scope.$watch('ratingValue', function (oldVal, newVal) {
                if (newVal) {
                    updateStars();
                }
            });
        }
    }
});